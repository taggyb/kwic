import java.io.BufferedReader;
import java.io.FileReader;

import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.StringTokenizer;

/*
 * Input parses in lines of text by reading from a text file
 */
public class Input {
	public void setLineStorage(String lineFile, LineStorage lines){
		try {
			BufferedReader reader = new BufferedReader(new FileReader(lineFile));
			String line = reader.readLine();
			
			while(line != null){
				line = line.toLowerCase();
				StringTokenizer tokenizer = new StringTokenizer(line);
				if(tokenizer.countTokens() > 0){
					lines.addEmptyLine();
				}
				while(tokenizer.hasMoreTokens()){
					lines.addWord(tokenizer.nextToken(), lines.getNumberOfLines() - 1);
				}
				line = reader.readLine();
			}
			reader.close();
		} catch(FileNotFoundException inputError) {
			System.err.println("KWIC Error: " + lineFile + " file not found.");
			System.exit(1);
		} catch(IOException inputError) {
			System.err.println("KWIC Error: Could not read " + lineFile + " file.");
			System.exit(1);
		}
	}
}

