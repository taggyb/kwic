import java.util.ArrayList;
import java.util.List;

/*
 * LineStorage stores lines of text which can be used for processing
 */
public class LineStorage {
	private List<ArrayList<String>> lines_ = new ArrayList<ArrayList<String>>();

	public void addWord(String addedWord, int lineIndex){
		lines_.get(lineIndex).add(addedWord);
	}

	public String getWord(int wordIndex, int lineIndex){
		return lines_.get(lineIndex).get(wordIndex);
	}

	public int getLengthOfLine(int lineIndex){
		return lines_.get(lineIndex).size();
	}

	public void addEmptyLine(){
		ArrayList<String> emptyLine = new ArrayList<String>();
		lines_.add(emptyLine);
	}

	public void addLine(String[] wordsToBeAdded){
		ArrayList<String> addedLine = new ArrayList<String>();
		for(int i = 0; i < wordsToBeAdded.length; i++){
			addedLine.add(wordsToBeAdded[i]);
		}
		lines_.add(addedLine);
	}

	public List<String> getLine(int lineIndex){
		List<String> wantedLine = lines_.get(lineIndex);
		return wantedLine;
	}

	public String getLineAsString(int lineIndex){
		List<String> wantedLine = lines_.get(lineIndex);
		String wantedLineAsString =  "";
		for(int i = 0; i < wantedLine.size(); i++){
			if(i == wantedLine.size() - 1){
				wantedLineAsString += wantedLine.get(i);
			} else {
				wantedLineAsString += wantedLine.get(i) + " ";
			}
		}
		return wantedLineAsString;
	}

	public int getNumberOfLines(){
		return lines_.size();
	}
}
